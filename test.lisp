
;;------------------------------------------------------------------
;;MATH FUNCTIONS
;;abs value

;;repl for abs
(defun abs-repl ()
  (format t "abs>")
  (write (abs (read)))
  (format t "~%")
  (abs-repl))

(defun abs (x)
  (cond
    ((< x 0) (* -1 x))
    ((>= x 0) x)))


;;3. 
;;length of two sides and hypotenuse

;;p - perpendicular
;;b - base
;;h - hypotenuse

(defun right-tri-repl ()
  (format t "enter three arguments seperated by spaces. eg: 3 4 5~%")
  (format t "right-tri>")
  (write (right-tri (read) (read) (read)))
  (right-tri-repl))

(defun right-tri (p b h)
  (cond
    ((equal (+ (* p p) (* b b)) (* h h)) t))) 

;;factorial of a number
(defun factorial-repl ()
  (format t "enter a single argument. eg: 5~%")
  (format t "factorial>")
  (write (factorial (read)))
  (factorial-repl))

(defun factorial (n)
  (factorial-helper n 1))

(defun factorial-helper (n num)
  (cond
    ((equal n 0) num)
    ((numberp n)
     (factorial-helper (- n 1) (* num n)))))


;;mod of a number, helper for gcd and lcm
(defun mod (x y)
  (cond
    ((equal x y) 0)
    ((< y x) (mod (- x y) y))
    ((> y x) x)))

;;repl for gcd
(defun gcd-repl ()
  (format t "enter a single argument. eg: 5~%")
  (format t "gcd>")
  (write (gcd (read)))
  (gcd-repl))

;;gcd   
(defun gcd (x y)
  (cond
    ((equal y 0) x)
    ((not (equal y 0))
     (gcd y (mod x y)))))
;;repl for lcm
(defun lcm-repl ()
  (format t "enter a single argument. eg: 5~%")
  (format t "lcm>")
  (write (lcm (read)))
  (lcm-repl))
;;lcm
(defun lcm (x y)
  (cond
    ((equal y 0) x)
    ((numberp x)
     (/ (* x y) (gcd x y)))))


;;--------------------------------------------------------
;;SET FUNCTIONS

;;set intersection repl
(defun intersection-repl ()
  (format t "enter two list arguments. eg: (1 2 3) (1 2)~%")
  (format t "intersection>")
  (write (intersection (read) (read)))
  (intersection-repl))

;;set intersection 
(defun intersection (xs ys)
  (intersection-helper xs ys '()))

(defun intersection-helper (xs ys helper-list)
	(cond 
		((null xs) helper-list)
		((member (car xs) ys)
			(intersection-helper (cdr xs) ys (cons (car xs) helper-list)))
		((not (member (car xs) ys))
		 (intersection-helper (cdr xs) ys helper-list))))

;;repl for insert
(defun insert-repl ()
  (format t "enter two atom, list arguments. eg: 5 (1 2)~%")
  (format t "insert>")
  (write (insert (read) (read)))
  (insert-repl))
;;insert
(defun insert (x xs)
  (nub (cons x xs)))

;;repl for member
(defun member-repl ()
  (format t "enter two atom, list arguments. eg: 5 (1 2)~%")
  (format t "member>")
  (write (member (read) (read)))
  (member-repl))

;;Set membership
(defun member (x xs)
  (cond
    ((null xs) nil)
    ((equal x (car xs)) t)
    ((atom x) (member1 x (cdr xs)))))

;;repl for difference
(defun difference-repl ()
  (format t "enter two list arguments. eg: (5 6) (1 2)~%")
  (format t "difference>")
  (write (difference (read) (read)))
  (difference-repl))

;;set difference
;;TODO
(defun difference (xs ys)
  (difference-helper xs ys '()))

;;(defun difference-helper (xs ys helper-list)
;;  (cond
;;    ((null xs) helper-list)
;;    ((member (car xs) ys)
;;     (difference-helper (cdr xs) ys helper-list))
;;    ((not (member (car xs) ys))
;;     (difference-helper (cdr xs) ys (cons (car xs helper-list))))))
  
;;repl for subsetp
(defun subsetp-repl ()
  (format t "enter two list arguments. eg: (5 6) (1 2)~%")
  (format t "subsetp>")
  (write (subsetp (read) (read)))
  (subsetp-repl))

(defun subsetp (xs ys)
  (cond
    ((null xs) t)
    ((member (car xs) ys)
     (subsetp (cdr xs) ys))))

;;repl for supersetp
(defun supersetp-repl ()
  (format t "enter two list arguments. eg: (5 6) (1 2)~%")
  (format t "supersetp>")
  (write (supersetp (read) (read)))
  (supersetp-repl))

(defun supersetp (xs ys)
  (cond
    ((null ys) t)
    ((member (car ys) xs)
     (supersetp xs (cdr ys)))))
;;-------------------------------------------------------------------------------------
;; LIST FUNCTIONS


;;repl for addtoend
(defun addtoend-repl ()
  (format t "enter two atom, list arguments. eg: 5 (1 2)~%")
  (format t "addtoend>")
  (write (addtoend (read) (read)))
  (addtoend-repl))

;;add an element to the end of the list
(defun addtoend (x xs)
	(reverse (cons x (reverse xs))))

;;repl for reverse
(defun reverse-repl ()
  (format t "enter a list argument. eg: (1 2 3)~%")
  (format t "reverse>")
  (write (reverse (read)))
  (reverse-repl))


;;reverse
(defun reverse (xs)
  (setq helper-list '())
  (reverse-helper xs helper-list))

(defun reverse-helper (xs helper-list)
  (cond
    ((null xs) helper-list)
    ((listp helper-list)
     (reverse-helper (cdr xs) (cons (car xs) helper-list)))))

;;repl for map
(defun map-repl ()
  (format t "enter a list, function argument. eg: (1 2 3) 'listp~%")
  (format t "map>")
  (write (map (read)))
  (map-repl))

;;map
(defun my-map (func lst)
  (cond
    ((null lst) nil)
    ((listp lst)
     (cons (funcall func (car lst)) (my-map func (cdr lst))))))

;;repl for filter
(defun filter-repl ()
  (format t "enter a list, function argument. eg: (1 2 (3 4)) 'atom~%")
  (format t "filter>")
  (write (filter (read)))
  (filter-repl))

;;filter
(defun filter (xs filterfn)
  (filter-helper xs filterfn '()))

(defun filter-helper (xs filterfn lst)
  (cond
    ((null xs) (reverse lst))
    ((funcall filterfn (car xs))
     (filter-helper (cdr xs) filterfn (cons (car xs) lst)))
    ((not (funcall filterfn (car xs)))
     (filter-helper (cdr xs) filterfn lst))))

;;repl for index
(defun index-repl ()
  (format t "enter two atom, list arguments. eg: 2 (1 2 3) ~%")
  (format t "index>")
  (write (index (read)))
  (index-repl))

;;index of
(defun index (x xs)
  (index-helper x xs 0))

(defun index-helper (x xs index)
  (cond
    ((equal x (car xs)) index)
    ((numberp index)
     (index-helper x (cdr xs) (+ 1 index)))))

;;repl for remove-all
(defun remove-all-repl ()
  (format t "enter two atom, list arguments. eg: 2 (1 2 3) ~%")
  (format t "remove-all>")
  (write (remove-all (read)))
  (remove-all-repl))

;;remove all

(defun remove-all (x xs)
  (remove-all-helper x xs '()))

(defun remove-all-helper (x xs helper-list)
  (cond
    ((null xs) helper-list)
    ((equal (car xs) x)
     (remove-all-helper x (cdr xs) helper-list))
    ((not (equal (car xs) x))
     (remove-all-helper x (cdr xs) (cons (car xs) helper-list)))))

;;repl for nub
(defun nub-repl ()
  (format t "enter a list argument. eg: (1 2 2 3) ~%")
  (format t "nub>")
  (write (nub (read)))
  (nub-repl))

;;remove duplicates
(defun nub (xs)
  (nub-helper xs '()))

(defun nub-helper (xs helper-list)
  (cond
    ((null xs) (reverse helper-list))
    ((member (car xs) helper-list)
     (nub-helper (cdr xs) helper-list))
    ((not (member (car xs) helper-list))
     (nub-helper (cdr xs) (cons (car xs) helper-list)))))

;;repl for append
(defun append-repl ()
  (format t "enter two list arguments. eg: (1 2) (1 2 3) ~%")
  (format t "append>")
  (write (append (read)))
  (append-repl))

;;append
(defun append (xs ys)
  ((null xs) ys)
  ((listp ys)
   (append (cdr xs) (cons (car xs) ys)))) 

;;perfectp make a list of factors and add everything
(defun factors (n)
  (factors-helper n (- n 1) '()))

(defun factors-helper (n factor lst)
  (cond
    ((equal factor 0) lst)
    ((equal 0 (mod n factor))
     (factors-helper n (- factor 1) (cons factor lst)))
    ((not (equal 0 (mod n factor)))
     (factors-helper n (- factor 1) lst))))

(defun add-all (lst)
  (add-all-helper lst 0))

(defun add-all-helper (lst num)
  (cond
    ((null lst) num)
    ((numberp num)
     (add-all-helper (cdr lst) (+ num (car lst))))))

;;repl for perfectp
(defun perfectp-repl ()
  (format t "enter a number argument. eg: 2 ~%")
  (format t "perfectp>")
  (write (perfectp (read)))
  (perfectp-repl))

(defun perfectp (num)
  (cond
    ((equal num (add-all (factors num))))))

;;repl for abundantp
(defun abundantp-repl ()
  (format t "enter a number argument. eg: 2 ~%")
  (format t "abundantp>")
  (write (abundantp (read)))
  (abundantp-repl))

(defun abundantp (num)
  (cond
    ((> (add-all (factors num)) num))))

;;repl for deficientp
(defun deficientp-repl ()
  (format t "enter a number argument. eg: 2 ~%")
  (format t "deficientp>")
  (write (deficientp (read)))
  (deficientp-repl))

(defun deficientp (num)
  (cond
    ((< (add-all (factos num)) num))))
   








     
