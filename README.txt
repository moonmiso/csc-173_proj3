CSC173_Proj3
Name: Muskaan Mendiratta
Assignment Number: Project 03, Unit 03
I did not collaborate with anyone on this project.

FILES:
test.lisp		all functions in this file

FUNCTIONS IMPLEMENTED:

MATH FUNCTIONS
abs
factorial
right-tri
gcd
lcm

SET FUNCTIONS
member
insert
intersection
union
subsetp
supersetp

LIST FUNCTIONS
append
reverse
map
nub
filter
addtoend
indexof
remove-all

REPL:
Since you will be using abcl, it launches its own repl. I made a repl for each function in the following format:

fname-repl
(eg: repl for factorial is 'factorial-repl')

However, the repls I made don't allow the user to quit from the repl. Please run my functions before you run the corresponding repls :'(

I am sorry for the way I did I/O in this project, and eventhough I have coded in Lisp before, I really never had to do I/O.

COMPILING:
I used Allegro CL to compile, so I got the PACKAGE-LOCKED error. If you are using Allegro CL instead of abcl like me, then you will have to manually override each function.

eg:
Restart actions (select using :continue):
  0: Set the FUNCTION definition of the name LCM anyway.
  1: retry the load of test
  2: skip loading test

In this case, just keep entering ':continue 0' for each function. But if you are compiling using abcl, it shouldn't be a problem anyway.

